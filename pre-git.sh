#!/usr/bin/env bash


# Remove symlinks
echo "~Removing Links~"
rm -rf ./MelonBread/x86_64/MelonBread.db ./MelonBread/x86_64/MelonBread.files

# Copy file to make them "git freindly"
echo "~Restoring Git Friendly Copy~"
cp ./MelonBread/x86_64/MelonBread.db.tar ./MelonBread/x86_64/MelonBread.db
cp ./MelonBread/x86_64/MelonBread.files.tar ./MelonBread/x86_64/MelonBread.files

echo "~All Done~"
