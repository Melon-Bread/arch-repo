#!/usr/bin/env bash


# Remove "Git Friendly" copies
echo "~Removing Git Copies~"
rm -rf ./MelonBread/x86_64/MelonBread.db ./MelonBread/x86_64/MelonBread.files

# Restore symlinks for repo-add
echo "~Restoring repo-add links~"
cd ./MelonBread/x86_64/
ln -s MelonBread.db.tar MelonBread.db
ln -s MelonBread.files.tar MelonBread.files

echo "~All Done~"
